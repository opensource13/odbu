const pino = require('pino')
const logger = pino({
  transport: {
    target: 'pino-pretty'
  },
  level: "debug"
})

module.exports = logger

const fs = require("fs")
const path = require("path")
const sqlite3 = require('sqlite3').verbose()

const logger = require("../infra/logger")

let db

function getDBName() {
  const name = `${process.env.DB_NAME ? process.env.DB_NAME : "odbu"}.sqlite3`
  return path.join(process.cwd(), name)
}

function getDB() {
  if (!db) {
    db = new sqlite3.Database(getDBName())
  }
  return db
}

function initDB() {
  logger.info(`Usando base ${getDBName()}`)
  const db = getDB()
  db.serialize(() => {
    db.run(`
      CREATE TABLE IF NOT EXISTS files (
        id TEXT, 
        driveId TEXT, 
        name TEXT, 
        path TEXT, 
        size REAL, 
        mimeType TEXT,
        createdDateTime TEXT
      )`)

    db.run(`
      CREATE TABLE IF NOT EXISTS current (
        id TEXT UNIQUE,
        size REAL,
        mimeType TEXT,
        updatedDateTime TEXT
      )`)
  })
}

function deleteDB() {
  fs.unlinkSync(getDBName())
}

class SqliteRepo {
  asPromsie(query) {
    return new Promise((resolve, reject) => {
      this.db.serialize(() => {
        query(resolve, reject)
      })
    })
  }
}


module.exports = {
  getDB,
  initDB,
  deleteDB,
  SqliteRepo
}
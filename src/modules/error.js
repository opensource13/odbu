class Custom extends Error {
  constructor(message, detail) {
    super(message)
    this.detail = detail
  }
}

module.exports = {
  Custom
}
class File {
  id
  driveId
  name
  path
  size
  mimeType
  createdDateTime

  toString() {
    return `${this.path}/${this.name} [${this.createdDateTime}]`
  }
}

class FileFromResponse extends File {
  constructor(data) {
    super()
    this.id = data.id
    this.driveId = data.parentReference.driveId
    this.name = data.name
    this.path = data.parentReference.path
    this.size = data.size
    this.mimeType = data.file.mimeType
    // this.download = data['@microsoft.graph.downloadUrl']
    this.createdDateTime = data.createdDateTime
  }
}


class SharedFileFromResponse extends File {
  constructor(data, path) {
    // the path is sent to have a better organization structure based in real directories
    super()
    this.id = data.id
    this.name = data.name
    this.path = path
    this.size = data.size
    this.mimeType = data.file.mimeType
    // this.download = data['@microsoft.graph.downloadUrl']
    this.createdDateTime = data.createdDateTime
    if (data.remoteItem) {
      this.driveId = data.remoteItem.parentReference.driveId
    } else {
      this.driveId = data.parentReference.driveId
    }
  }
}

const isAFile = (v) => v['@microsoft.graph.downloadUrl']!== undefined

class Directory {
  id
  name
  childCount
  createdDateTime

  toString() {
    return `${this.name}(${this.childCount}) ${this.createdDateTime}`
  }
}

class OwnDirectory extends Directory {
  constructor(data) {
    super()
    this.id = data.id
    this.name = data.name
    this.childCount = data.folder.childCount
    this.createdDateTime = data.createdDateTime
  }
}

class SharedDirectory extends Directory {
  driveId

  constructor(data) {
    super()
    this.id = data.id
    this.name = data.name
    this.childCount = data.folder.childCount
    this.createdDateTime = data.createdDateTime
    if (data.remoteItem) {
      this.driveId = data.remoteItem.parentReference.driveId
    } else {
      this.driveId = data.parentReference.driveId
    }
  }
}

const isADirectory = (v) => v['folder']!== undefined

const convertPathFromUrl = (url) => {
  const urlSlices = url.split("https://")
  const domainPathSlices = urlSlices[urlSlices.length - 1].split("?")[0]
  const subPath =  domainPathSlices.substring(0, domainPathSlices.lastIndexOf("/"))
  return decodeURI(subPath)
}

const fixSharedPath = (parentPath, driveIdPath) => {
  const subPath = parentPath.split("root:/")
  const subPaths = subPath[subPath.length -1].split("/")
  let startIndex = 0 
  for(startIndex; startIndex< subPaths.length; startIndex++) {
    if (!driveIdPath.includes(subPaths[startIndex])) {
      break
    }
  }
  const realSubPath = subPaths.slice(startIndex, subPaths.length).join("/")
  return `${driveIdPath}/${realSubPath}`
}

module.exports = {
  File,
  FileFromResponse,
  SharedFileFromResponse,
  isAFile,
  OwnDirectory,
  SharedDirectory,
  Directory,
  isADirectory,
  convertPathFromUrl,
  fixSharedPath
}
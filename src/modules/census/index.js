const Infra = require("../infra")
const App = require("./app")
const Shared = require("./app/Shared")
const logger = require("../../infra/logger")


async function Start(client, shared=false, dbRepository=undefined) {

  try {
    if(!shared) { // return personal files
      logger.info("Consultando archivos personales")
      const files = await App.GetAllFiles(
        Infra.rootRetriever(client),
        Infra.itemRetriever(client)
      )
      return files
    } else { // return shared files
      logger.info("Consultando archivos compartidos")
      
      let files = []
      await Shared.GetAllFiles(
        Infra.sharedRetriever(client),
        Infra.sharedDirectoryRetriever(client),
        async (detectedFiles) => {
          files = files.concat(detectedFiles)
          // console.log(`${files.length} archivos ...`)
          if (dbRepository) {
            await dbRepository.saveFiles(detectedFiles)
          }
        }
      )
      
      const memorySize = files.reduce((partial, f) => partial + f.size, 0)
      logger.info(`${files.length} archivos detectados ${memorySize} bytes -> ${memorySize/Math.pow(1024,3)} Gb`)
      
      const count = await dbRepository.countFiles()
      const dbWeight = await dbRepository.weightFiles()
      logger.info(`${count} registrados en base ${dbWeight} bytes -> ${dbWeight/Math.pow(1024,3)} Gb`)
      return files
    }
  } catch (e) {
    logger.error(e)
  }
}


module.exports = {
  Start
}

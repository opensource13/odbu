const { File } = require("../domain")
const { SqliteRepo } = require("../../../infra/sqlite3")


class CensusRepo extends SqliteRepo {
  db
  constructor(db) {
    super()
    this.db = db
  }

  clean() {
    return this.asPromsie((resolve, _) => {
      this.db.run("DELETE FROM files");
      resolve()
    })
  }

  saveFiles(files) {
    return this.asPromsie((resolve, _) => {
      const values = files.map(file => [file.id, file.driveId, file.name, file.path, file.size, file.mimeType, file.createdDateTime])
      const stmt = this.db.prepare("INSERT INTO files VALUES (?, ?, ?, ?, ?, ?, ?)");
      for (const file of values) {
        stmt.run(file);
      }
      stmt.finalize((err) => {
        if (err) { reject(err)}
        resolve()
      });
    })
  }

  countFiles() {
    return this.asPromsie((resolve, reject) => {
      this.db.get("SELECT COUNT(*) FROM files", function(err, row) {
        if (err) { reject(err) }
        resolve(row["COUNT(*)"])
      });
    })
  }

  weightFiles() {
    return this.asPromsie((resolve, reject) => {
      this.db.get("SELECT sum(size) as WEIGHT FROM files", function(err, row) {
        if (err) { reject(err) }
        resolve(row["WEIGHT"])
      })
    })
  }

  getFiles(offset, limit) {
    console.log(offset, limit)
    return this.asPromsie((resolve, reject) => {
      const files = []
      this.db.all(`
        SELECT id, driveId, name, path, size, mimeType, createdDateTime
        FROM files
        LIMIT ${limit} OFFSET ${offset}
      `, function(err, rows) {
        if (err) { 
          console.log(err)
          reject(err)
        }
        rows.forEach(row => {
          const newFile = new File()
          newFile.id = row.id
          newFile.driveId = row.driveId
          newFile.name = row.name
          newFile.path = row.path
          newFile.size = row.size
          newFile.mimeType = row.mimeType
          newFile.createdDateTime = row.createdDateTime
          files.push(newFile)
        })
        resolve(files)
      })
    })
  }

  getMissingFiles() {
    return this.asPromsie((resolve, reject) => {
      const files = []
      this.db.all(`
        SELECT files.id as id, driveId, name, path, files.size as size, files.mimeType as mimeType, createdDateTime 
          FROM files
          LEFT JOIN current ON current.id = files.id
          WHERE current.id IS NULL
      `, function(err, rows) {
        if (err) { reject(err) }
        rows.forEach(row => {
          const newFile = new File()
          newFile.id = row.id
          newFile.driveId = row.driveId
          newFile.name = row.name
          newFile.path = row.path
          newFile.size = row.size
          newFile.mimeType = row.mimeType
          newFile.createdDateTime = row.createdDateTime
          files.push(newFile)
        })
        resolve(files)
      })
    })
  }

}

module.exports = CensusRepo

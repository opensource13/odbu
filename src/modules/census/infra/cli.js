const { Table } = require('console-table-printer')
const logger = require("../../../infra/logger")
const prompts = require("prompts")

// Infra Layer
const Auth = require("../../../cli/auth")
const Census = require("..")


function CmdOptions(yargs) {
  return yargs
    .option("p", { 
      alias: "persist",
      type: "boolean",
      describe: "Permite guardar el resultado en la base sqlite3"
    })
    .option("e", { 
      alias: "extended",
      type: "boolean",
      describe: "Versioón extendida de la impresión"
    })
}

function Cmd(censusRepo) {
  return async function (yargs) {
    try {
      let client = await Auth.getClient(yargs)

      
      if(yargs.persist) {
        const count = await censusRepo.countFiles()
        
        if (count != 0) {
          let selected = await NotEmptyAction()
          if (selected.action === "cancel") return
        }
        await Census.Start(client, yargs.shared, censusRepo)
      } else {
        await Census.Start(client, yargs.shared)
      }

    } catch(e) {
      console.log(e)
      return false
    }
  }
}

async function NotEmptyAction () {
  return prompts({
    type: 'select',
    name: 'action',
    message: 'La base parece no estar vacía y seleccionó la opción guardar, ¿qué deseas hacer?',
    choices: [
      {title: "Agregar registros (puede sobreescribir)", value: "override"},
      {title: "Cancelar el guardado", value: "cancel"}
    ]
  });
} 

function printFilesTable(files=[], extended=false) {
  const p = new Table()
  files.forEach(r => {
    if (extended) {
      p.addRow({id: r.id, name: r.name, path: r.path, size: r.size, mimeType: r.mimeType, created: r.createdDateTime})
    } else {
      p.addRow({id: r.id, driveId: r.driveId, name: r.name, path: r.path, created: r.createdDateTime})
    }
  })
  p.printTable()
}


module.exports = {
  CmdOptions,
  Cmd,
  printFilesTable
}
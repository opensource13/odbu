class Service {
  repository

  constructor(repository) {
    this.repository = repository
  }

  async save(files=[]) {
    await this.repository.saveFiles(files)
  }

  async count() {
    return await this.repository.countFiles()
  }

  async getFiles(offset, limit) {
    return await this.repository.getFiles(offset, limit)
  }

  async getMissing() {
    return await this.repository.getMissingFiles()
  }
}

module.exports = {
  Service
}
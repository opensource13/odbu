const logger = require("../../../infra/logger")
const Domain = require("../domain")
const Error = require("../../error")
const Utils = require("../../utils")
const url = require("url")



const DirDict = {}

function addToDirDict(driveId, webUrl) {
  if (DirDict[driveId] === undefined) {
    const finalPath = Domain.convertPathFromUrl(webUrl)
    DirDict[driveId] = finalPath
  }
}

async function GetAllFiles(sharedRetriever, directoryRetriever, onFilesDetectedCallback) {
  const files = []
  let directories = []
  // Try to get shared files
  try {
    const res = await sharedRetriever
  
    res.value.forEach((item) => {
      const url = item.remoteItem.webUrl
      if (item.file != undefined) {
        const newFile = new Domain.SharedFileFromResponse(item, Domain.convertPathFromUrl(url))
        files.push(newFile)
        logger.info(newFile.toString())
      } else if (item.folder != undefined) {
        directories.push(new Domain.SharedDirectory(item))
        const driveId = item.remoteItem.parentReference.driveId
        addToDirDict(driveId, url)
      }
    })
  } catch(e) {
    throw new Error.Custom("[🖥️] -> Error  root drive", e)
  }
  
  await onFilesDetectedCallback(files)

  try {

    // const subDirectoriesSlices = Utils.createChunks(directories, process.env.CHUNK_SIZE)
    // const totalChunks = subDirectoriesSlices.length
    
    // logger.info(`Se procesarán ${totalChunks} bloque(s) de subdirectorios`)
    
    // for (let i=0; i<totalChunks; i++) {
    //   logger.info(`Procesando bloque  ${i+1} / ${totalChunks}`)
    //   await getSubdirectories(subDirectoriesSlices[i], directoryRetriever, onFilesDetectedCallback)
    // }

  
    for(let subD=0; subD<directories.length; subD++) {
      logger.info(`📂 Subdirectory ${directories[subD].name} (${subD+1}/${directories.length})`)
      await getSubdirectories([directories[subD]], directoryRetriever, onFilesDetectedCallback)
    }

  } catch(e) {
    throw new Error.Custom("[📁] -> Error fetching shared subDirectory", e)
  }

  return files
}

// Recursively get all subdirectories
async function getSubdirectories(subDirectories, itemRetriever, callback) {
  const allCalls = []
  let tmpSub = []
  let tmpFiles = []
  let nextLink

  subDirectories.forEach(d => {
    allCalls.push(itemRetriever(d.driveId, d.id))
  })

  const allRes = await Promise.allSettled(allCalls)

  for (let i=0; i < allRes.length; i++) {
    const res = allRes[i]
    if (res.status === "fulfilled") { 
      const responseValue = res.value.value
      nextLink = res.value["@odata.nextLink"]
      
      responseValue.forEach(item => {
        if (Domain.isAFile(item)) {
          const driveId = item.parentReference.driveId
          const finalPath = Domain.fixSharedPath(item.parentReference.path, DirDict[driveId])
          const newFile = new Domain.SharedFileFromResponse(item, finalPath)
          logger.info(`📂 ${newFile.toString() }`)
          tmpFiles.push(newFile)
        } else if (Domain.isADirectory(item)) {
          tmpSub.push(new Domain.SharedDirectory(item))
        }
      })

      await callback(tmpFiles)

      if (nextLink !== undefined) {
        const aditionalSubDirs = await getNextLink(nextLink, itemRetriever, callback)
        tmpSub = tmpSub.concat(aditionalSubDirs)
      }

    } else {
      logger.error(`Error en la inspección del subdirectorio ${subDirectories[i].name} request status: ${res.status}`)
      logger.error(res)
    }
  }

  if (tmpSub.length > 0) {
    return await getSubdirectories(tmpSub, itemRetriever, callback)
  }

  return true
}

/*
* Function to follow next links (pagination) of the graph api
*/
async function getNextLink(link, itemRetriever, callback) {
  // Expected link (string) 
  // https://graph.microsoft.com/v1.0/drives/b!n39f3ZtNkk2ZFNt3I96bpMltQPhVHmlDsbJUKKyjzmqi2tS5D-1kSbcmLfmIRYTO/items/01NPVIV3EJCA4RWBDIB5F3FLVMYOHLBILE/children?top=1000&$skiptoken=UGFnZWQ9VFJVRSZwX0ZpbGVMZWFmUmVmPVNlc2lvbitDaXRhK1NBVC0yMDIzMDEwOV8xNDE4MjMtR3JhYmFjaSVjMyViM24rZGUrbGErcmV1bmklYzMlYjNuLm1wNCZwX0lEPTMwNDg
  const parsed = url.parse(link, true)
  const tmpSub = []
  let tmpFiles = []

  let aditionalSubDirs = []

  const top = parsed.query["top"]
  const skipToken = parsed.query["$skiptoken"]
  const [driveId, itemId] = parsed.pathname.substr(0,parsed.pathname.length-9).split("/drives/")[1].split("/items/")

  const res = await itemRetriever(driveId, itemId, top, skipToken)
  const nextLink = res["@odata.nextLink"]
  const responseValue = res.value

  responseValue.forEach(item => {
    if (Domain.isAFile(item)) {
      const driveId = item.parentReference.driveId
      const finalPath = Domain.fixSharedPath(item.parentReference.path, DirDict[driveId])
      const newFile = new Domain.SharedFileFromResponse(item, finalPath)
      logger.info(`📂 ${newFile.toString() }`)
      tmpFiles.push(newFile)
    } else if (Domain.isADirectory(item)) {
      tmpSub.push(new Domain.SharedDirectory(item))
    }
  })

  await callback(tmpFiles)

  if (nextLink !== undefined) {
    aditionalSubDirs = await getNextLink(nextLink, itemRetriever, callback)
  }

  return tmpSub.concat(aditionalSubDirs)

}

module.exports = {
  GetAllFiles
}
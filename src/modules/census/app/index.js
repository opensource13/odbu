const logger = require("../../../infra/logger")
const Domain = require("../domain")
const Error = require("../../error")

const files = []
const directories = []

async function GetAllFiles(rootRetriever, itemRetriever) {

  // Try to get te root drive
  try {
    const res = await rootRetriever
    res.value.forEach((item) => {
      if (Domain.isAFile(item)) {
        files.push(new Domain.FileFromResponse(item))
      } else if (Domain.isADirectory(item)) {
        directories.push(new Domain.OwnDirectory(item))
      }
    })  
  } catch(e) {
    throw new Error.Custom("[🖥️] -> Error fetching root drive", e)
  }

  try {
    await getSubdirectories(directories, itemRetriever)
  } catch(e) {
    throw new Error.Custom("[📁] -> Error fetching sub directory", e)
  }

  return files
}

async function getSubdirectories(subDirectories, itemRetriever) {
  const allCalls = []
  const tmpSub = []
  subDirectories.forEach(d => {
    allCalls.push(itemRetriever(d.id))
  })

  const allRes = await Promise.allSettled(allCalls)
  allRes.forEach(res => {
    if (res.status === "fulfilled") { 
      res.value.value.forEach(item => {
        if (Domain.isAFile(item)) {
          files.push(new Domain.FileFromResponse(item))
        } else if (Domain.isADirectory(item)) {
          tmpSub.push(new Domain.OwnDirectory(item))
        }
      })
    } else {
      logger.error(`Error in request`)
      console.log(res)
    }
  })
    

  if (tmpSub.length > 0) {
    return getSubdirectories(tmpSub, itemRetriever)
  }

  return true
}



module.exports = {
  GetAllFiles
}
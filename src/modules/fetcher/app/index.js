const logger = require("../../../infra/logger")
const path = require("path")
const { https } = require('follow-redirects')
const fs = require("fs")
const Utils = require("../../utils")
const extend = require('util')._extend

async function Start(files=[], itemRetriever, onSaved=(f) => {}) {
  const filesSlices = Utils.createChunks(files, process.env.CHUNK_SIZE)
  const chunksLength = filesSlices.length

  logger.info(`Procesando ${chunksLength} bloque(s)`)
 
  for (let i=0; i<chunksLength; i++) {
    const fileSlice = filesSlices[i]
    logger.info(`Procesando bloque  ${i+1} / ${chunksLength}`)
    const allFilesLinkRequests = fileSlice.map(file => itemRetriever(file.driveId, file.id))
    const allFilesLinkResponse = await Promise.allSettled(allFilesLinkRequests)
    const allFilesLink = allFilesLinkResponse.map((res, i) => {
      if (res.status === "fulfilled") {
        const fileLink = extend({download: res.value['@microsoft.graph.downloadUrl']}, fileSlice[i])
        return fileLink
      } else {
        logger.warn(res)
        logger.warn(`No se pudo obtener la url del archivo con driveID ${fileSlice[i].driveId} id ${fileSlice[i].id} `)
      }
    })

    logger.info(`\t${allFilesLink.length} urls obtenidas`)

    const allReqquests = allFilesLink.map((file) => createRequestPromise(file, onSaved))

    const fullRes = await Promise.allSettled(allReqquests)

    const errors = fullRes.filter( r => r.status === "rejected") 
    errors.forEach((e) => {
      console.log(e.reason)
    })
  }

}

function createRequestPromise(file, onSaved, retries=0) {
  return new Promise((resolve, reject) => {

    if (retries >= 1) {
      return reject(`Limite de reintentos para id ${file.id}`)
    }

    const BASE = process.env.DOWNLOAD_DIR ? process.env.DOWNLOAD_DIR : "downloads"

    let basePath = path.join(BASE, file.path)
    let filePath = path.join(basePath, file.name)

    if (!fs.existsSync(basePath)){
      fs.mkdirSync(basePath, { recursive: true })
    }

    https.get(file.download, response => {

      if (response.statusCode === 429) {
        const retry = response.headers["retry-after"]
        reject(`Debe esperar ${retry} seg para reintentar la descarga de ${file.download}`)
        return
        // const message = `Reintentar en ${retry} para ${file.id}`
        // logger.warn(message)
        // setTimeout(() => {
        //   logger.info(`Reintento ${retries+1} de 1 ${file.id} después de ${retry} seconds`)
        //   createRequestPromise(file, onSaved, retries +1)
        //     .then(_ => { resolve(file) })
        //     .catch(e => {
        //       reject(`Se reintentó después de ${retry} seg pero no se pudo obtener ${file.id}`)
        //       return
        //     })
        // }, (retry*1000)+1)
        // return
      }

      if (response.statusCode != 200) {
        reject(`Se obtuvo un código ${response.statusCode} para el diveId ${file.driveId}} id ${file.id}: ${file.name} \n\t- ${file.download}`)
      }

      const responseSize = response.headers["content-length"]
      const responseMimeType = response.headers["content-type"]

      if (file.size != responseSize) {
        const message = `Tamaño de la descarga incongruente, se esperaba ${file.size} pero se recibió ${responseSize} para el id ${file.id} ${responseSize} ${responseMimeType}`
        logger.error(message)
        reject(message)
        return
      }
      if (file.mimeType != responseMimeType) {
        const message = `Tipo de archivo de la descarga incongruente, se esperaba ${file.mimeType} pero se recibió ${responseMimeType} para el id ${file.id} ${responseMimeType}`
        logger.warn(message)
        // reject(message)
        return
      }
      const newFile = fs.createWriteStream(filePath)
      response.pipe(newFile)
      newFile.on('finish', () => {
        file.size = responseSize
        file.mimeType = responseMimeType
        newFile.close()
        onSaved(file)
        resolve(file)
      });
    }).on('error', err => { 
      fs.unlink(filePath, () => {})
      reject(err.message)
    });

  })
}

module.exports = {
  Start
}
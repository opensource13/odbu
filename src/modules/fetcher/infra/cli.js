// Infra Layer
const logger = require("../../../infra/logger")
const Auth = require("../../../cli/auth")
const Infra = require("../../infra")
// Application Layer
const Census = require("../../census")
const Fetcher = require("../app")
const File = require("../../census/app/File")


function CmdOptions(yargs) {
  return yargs
    .option("v", { 
      alias: "verbose",
      type: "boolean",
      describe: "Imprime el detalle del proceso"
    })
    .option("f", { 
      alias: "force",
      type: "boolean",
      describe: "Sobre escribe la lista de archivos a descargar"
    })
    .option("r", { 
      alias: "range",
      type: "string",
      describe: "Límite de archivos a consultar separados por ':'"
    })
}

function Cmd(censusRepo, fetcherRepo) {
  let counter = 0
  let files = []

  return async function (yargs) {
    async function onSavedCallback(file) {
      try {
        await fetcherRepo.saveLast(file)
        counter ++
        if(yargs.verbose) logger.info(`\tArchivo ${counter}/${files.length} ${file.name} guardado exitosamente`)
      } catch (e) {
        logger.warn(`${e}`)
      }
    }

    let client = await Auth.getClient(yargs)

    const startTime = new Date()
    const fileSvc = new File.Service(censusRepo)
    const filesCount = await fileSvc.count()

    
    if (filesCount === 0) {
      files = await Census.Start(client, yargs.shared)
      await fileSvc.save(files)
    } else {
      logger.info(`Se encontraron ${filesCount} archivos en la base`)
      if(yargs.force) {
        logger.info("Sobre escribiendo base ...")
        files = await Census.Start(client)
        await fileSvc.save(files, true)
        await fetcherRepo.clean()
      }
    }
    
    limit = files.length

    if (yargs.range) {
      const [offset, limit] = yargs.range.split(":")
      files = await fileSvc.getFiles(offset, limit)
      logger.info(`Descarga limitada de ${offset} a ${limit}`)
    } else {
      files = await fileSvc.getMissing()
    }

    if (files.length > 0) {
      logger.info(`Obteniendo ${files.length} archivos pendientes`)
      const itemDirectoryRetriever = Infra.sharedItemRetriever(client)
      await Fetcher.Start(files, itemDirectoryRetriever, onSavedCallback)
    } else {
      logger.info(`Los archivos ya se descargaron`) 
    }
    const endTime = new Date()
    logger.info(`Terminado en ${(endTime - startTime)/1000} s`)
  }
}



module.exports = {
  CmdOptions,
  Cmd
}
const { SqliteRepo } = require("../../../infra/sqlite3")

class FetcherRepo extends SqliteRepo {
  db
  constructor(db) {
    super()
    this.db = db
  }

  clean() {
    return this.asPromsie((resolve, _) => {
      this.db.run("DELETE FROM current", [], (err) => {
        if (err) {
          reject(err)
        }
        resolve()
      });
    })
  }


  saveLast(file) {
    return this.asPromsie((resolve, reject) => {
      this.db.run(
          "INSERT INTO current(id, size, mimeType, updatedDateTime) VALUES (?, ?, ?, ?)", 
          [file.id, file.size, file.mimeType, new Date().toISOString()], 
          (err) => {
            if (err) {
            reject(`${err} ${file.id}`)
          }
          resolve()
      })
    })
  }

  countFiles() {
    return this.asPromsie((resolve, reject) => {
      this.db.get("SELECT COUNT(*) FROM current", function(err, row) {
        if (err) { reject(err) }
        resolve(row["COUNT(*)"])
      });
    })
  }

 
}

module.exports = FetcherRepo
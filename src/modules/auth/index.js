const { Client, HTTPMessageHandler, AuthenticationHandler } = require("@microsoft/microsoft-graph-client")

const { UsernamePasswordCredential, DeviceCodeCredential } = require("@azure/identity")
const { TokenCredentialAuthenticationProvider } = require("@microsoft/microsoft-graph-client/authProviders/azureTokenCredentials")


const TENANT_ID=process.env.TENANT_ID
const CLIENT_ID=process.env.CLIENT_ID


function ByCredentials(debug=false) {
  const ONEDRIVE_USER=process.env.ONEDRIVE_USER
  const ONEDRIVE_PASS=process.env.ONEDRIVE_PASS

  const credential = new UsernamePasswordCredential(TENANT_ID, CLIENT_ID, ONEDRIVE_USER, ONEDRIVE_PASS)

  return getGetClient(credential, debug)
}

function ByDevice(debug=false) {
  const credential = new DeviceCodeCredential({ tenantId: TENANT_ID, clientId: CLIENT_ID});

  return getGetClient(credential, debug)
}

function getGetClient(credential, debug=false) {
  const authProvider = new TokenCredentialAuthenticationProvider(credential, {
    scopes: ["Files.Read.All", "Files.ReadWrite.All", "Sites.Read.All", "Sites.ReadWrite.All"]
  });

  if (debug) {
    const loggingHandler = new CustomLoggingHandler()
    const authHandler = new AuthenticationHandler(authProvider)
    const httpHandler = new HTTPMessageHandler();
    authHandler.setNext(loggingHandler);
    loggingHandler.setNext(httpHandler);
    return Client.initWithMiddleware({debugLogging: true, middleware: authHandler})
  }

  return Client.initWithMiddleware({debugLogging: false, authProvider})
}


class CustomLoggingHandler {
  nextMiddleware = null;

  execute = async (context)  => {
    // console.log(context.options.headers)
    return await this.nextMiddleware.execute(context);
  }
  setNext = (middleware) => {
    this.nextMiddleware = middleware;
  }
}

module.exports = {
  ByCredentials,
  ByDevice
}
require("cross-fetch")

function rootRetriever(client) {
  return client.api('/me/drive/root/children').get()
}

function itemRetriever(client) {
  return (id) => {
    return client.api(`/me/drive/items/${id}/children`).get()
  }
}

function itemContentRetriever(client) {
  return (id) => {
    return client.api(`/me/drive/items/${id}/content`).get()
  }
}

// Obtiene la lista de elementos compartidos comigo (directorios y archivos raiz) 
function sharedRetriever(client) { 
  return client.api(`/me/drive/sharedWithMe`).get()
}

function sharedDirectoryRetriever(client) {
  /*
  * top -> amount of elements to retrieve
  * skiptoken -> offset of elements
  */
  return (remoteItemDriveId, remoteItemId, top=300, skipToken=undefined) => {
    let url = `/drives/${remoteItemDriveId}/items/${remoteItemId}/children?top=${top}`
    if (skipToken) {
      url = `${url}&$skiptoken=${skipToken}`
    }
    return client.api(url).get()
  }
}


function sharedItemRetriever(client) {
  return (remoteItemDriveId, remoteItemId) => {
    return client.api(`/drives/${remoteItemDriveId}/items/${remoteItemId}`).get()
  }
}


module.exports = {
  rootRetriever,
  itemRetriever,
  itemContentRetriever,
  sharedItemRetriever,
  sharedDirectoryRetriever,
  sharedRetriever
}
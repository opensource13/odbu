function createChunks(source=[], chunkSize) {
  const sourceLength = source.length
  const slices = []

  if (sourceLength === 0) return slices

  const CHUNK_SIZE = chunkSize !== undefined ? parseInt(chunkSize): sourceLength
  const chunks = Math.ceil(sourceLength/ CHUNK_SIZE)

  for (let i=0; i<chunks; i++) {
    const start = i*CHUNK_SIZE
    const end = start+CHUNK_SIZE > sourceLength ? sourceLength : start+CHUNK_SIZE
    const sourceSlice = source.slice(start, end)
    slices.push(sourceSlice)
  }

  return slices

}

module.exports = {
  createChunks
}
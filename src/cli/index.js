#!/usr/bin/env node
const yargs = require('yargs')
const { hideBin } = require('yargs/helpers')
const argv = yargs(hideBin(process.argv)).argv
const ConfigFile = require("./config")

require('dotenv').config()

if(argv.c) {
  ConfigFile.setEnvData(argv.c)
}

const SQLITE = require("../infra/sqlite3")

const List = require("../modules/census/infra/cli")
const Fetch = require("../modules/fetcher/infra/cli")


const CensusRepo = require("../modules/census/infra/sqlite3")
const FetcherRepo = require("../modules/fetcher/infra/sqlite3")

let censusRepo
let fetcherRepo

async function main() {

  SQLITE.initDB()
  censusRepo = new CensusRepo(SQLITE.getDB())
  fetcherRepo = new FetcherRepo(SQLITE.getDB())

  parseCmd()
}


function parseCmd() {
  yargs
    .option("d", { 
      alias: "debug",
      type: "boolean",
      describe: "Imprime el detalle del proceso de forma más completa"
    })
    .option("c", { 
      alias: "config",
      type: "string",
      describe: "Indica la ruta del archivo con información de configuración, sobre esrive las variables de entorno"
    })
    .option("s", { 
      alias: "shared",
      type: "boolean",
      describe: "Consulta los archivos compartidos"
    })
    .command(
      "list", 
      "Muestra todos los archivos en la cuenta de OneDrive",
      List.CmdOptions,
      List.Cmd(censusRepo)
    )
    .command(
      "fetch",
      "Obtiene los archivos en OneDrive y los descarga",
      Fetch.CmdOptions,
      Fetch.Cmd(censusRepo, fetcherRepo)
    ).command(
      "purge",
      "Elimina la base se sqlite3",
      SQLITE.deleteDB
    )
    .parse()
}

main()


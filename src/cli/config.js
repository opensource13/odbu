const fs = require("fs")
const path = require("path")
const logger = require("../infra/logger")


function setEnvData(c_path) {
  const configPath = path.join(process.cwd(), c_path)
  if (!fs.existsSync(configPath)) {
    logger.error(`El archivo de configuración no existe: ${configPath}`)
    process.exit(1)
  }

  const data = fs.readFileSync(configPath, 'utf8')
  const jsonInfo = JSON.parse(data)

  logger.info(`Usando el archivo de configuración ${configPath}`)

  if(jsonInfo.TENANT_ID) process.env.TENANT_ID = jsonInfo.TENANT_ID
  if(jsonInfo.CLIENT_ID) process.env.CLIENT_ID = jsonInfo.CLIENT_ID
  if(jsonInfo.ONEDRIVE_USER) process.env.ONEDRIVE_USER = jsonInfo.ONEDRIVE_USER
  if(jsonInfo.ONEDRIVE_PASS) process.env.ONEDRIVE_PASS = jsonInfo.ONEDRIVE_PASS
  if(jsonInfo.DOWNLOAD_DIR) process.env.DOWNLOAD_DIR = jsonInfo.DOWNLOAD_DIR
  if(jsonInfo.CHUNK_SIZE) process.env.CHUNK_SIZE = jsonInfo.CHUNK_SIZE
  if(jsonInfo.DB_NAME) process.env.DB_NAME = jsonInfo.DB_NAME
}

module.exports = { setEnvData }
const prompts = require("prompts")
const Auth = require("../modules/auth")

async function askAuthStrategy() {
  const response = await prompts({
    type: 'select',
    name: 'authBy',
    message: 'Elije el modo de autenticar',
    choices: [
      {title: "Dispositivo", value: "device"},
      {title: "Credenciales", value: "credentials"}
    ]
  });

  if (response.authBy === "credentials") {
    const credentials = await prompts([
      {
        type: 'text',
        name: 'user',
        message: 'Correo de OneDrive',
      }, {
        type: 'password',
        name: 'password',
        message: 'Contraseña de OneDrive',
      }
    ]);
    return {
      authBy: response.authBy,
      credentials
    }
  }

  return response
}

async function getClientByPrompt (debug) {
  const promptRes = await askAuthStrategy()
  if (promptRes.authBy === "device") {
    return Auth.ByDevice(debug)
  } else if (promptRes.authBy === "credentials") {
    process.env['ONEDRIVE_USER'] = promptRes.credentials.user
    process.env['ONEDRIVE_PASS'] = promptRes.credentials.password
    return Auth.ByCredentials(debug)
  }
}

async function getClient(yargs) {
  if (process.env.ONEDRIVE_PASS && process.env.ONEDRIVE_USER) {
    return Auth.ByCredentials(yargs.debug)
  } 
  return await getClientByPrompt(yargs.debug)
}


module.exports = {
  getClient
}
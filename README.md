# ☁️ One Drive Backup

Cliente escrito en Js basado en el SDk de microsoft-graph para backup de directorios de One Drive.


## 📖 Requisitos

### Aplicación en Active Directory
Para poder usar el cliente, es importante registarr una aplicación en el Active Directory dentro de la cuenta de Azure a respaldar.

Puede ver la referencia de los pasos a seguir en la [documentación oficial](https://learn.microsoft.com/es-es/onedrive/developer/rest-api/getting-started/app-registration?view=odsp-graph-online#register-your-app-with-microsoft).

### Node JS

Esta aplicación fue escrita y probaada con [NodeJs v18.16.0](https://nodejs.org/dist/v18.16.0/)


##  💻 Usage

Si clona el repositorio, puede hacer una "instalación" con el comando.

```bash
$ npm link
```

El programa se apoya de una base sqlite para guardar tanto el listado de archivos a descargar así como los archivos guardados durante un proceso. Eso permite que si se interrumpe la descarga, la siguiente vez que se ejecute, retmomará el proceso ignorando los archivos ya existentes.

```bash
$ odbu --help
odbu [command]

Comandos:
  odbu list   Muestra todos los archivos en la cuenta de OneDrive
  odbu fetch  Obtiene los archivos en OneDrive y los descarga
  odbu purge  Elimina la base se sqlite3

Opciones:
      --help     Muestra ayuda                                        [booleano]
      --version  Muestra número de versión                            [booleano]
  -d, --debug    Imprime el detalle del proceso de forma más completa [booleano]
  -c, --config   Indica la ruta del archivo con información de configuración,
                 sobre escribe las variables de entorno   [cadena de caracteres]
  -s, --shared   Consulta los archivos compartidos                    [booleano]
```


Se cuentan con tres comandos, **list**, **fetch** y **purge**, cada comando tiene sus propias opciones y puede usar la bandera `--help` en cada uno para obtener más información.

```bash
$ odbu list --help
odbu list

Muestra todos los archivos en la cuenta de OneDrive

Opciones:
      --help      Muestra ayuda                                       [booleano]
      --version   Muestra número de versión                           [booleano]
  -d, --debug     Imprime el detalle del proceso de forma más completa[booleano]
  -c, --config    Indica la ruta del archivo con información de configuración,
                  sobre escribe las variables de entorno  [cadena de caracteres]
  -s, --shared    Consulta los archivos compartidos                   [booleano]
  -p, --persist   Permite guardar el resultado en la base sqlite3     [booleano]
  -e, --extended  Versioón extendida de la impresión                  [booleano]
```

```bash
$ odbu fetch --help
odbu fetch

Obtiene los archivos en OneDrive y los descarga

Opciones:
      --help     Muestra ayuda                                        [booleano]
      --version  Muestra número de versión                            [booleano]
  -d, --debug    Imprime el detalle del proceso de forma más completa [booleano]
  -c, --config   Indica la ruta del archivo con información de configuración,
                 sobre escribe las variables de entorno   [cadena de caracteres]
  -s, --shared   Consulta los archivos compartidos                    [booleano]
  -v, --verbose  Imprime el detalle del proceso                       [booleano]
  -f, --force    Sobre escribe la lista de archivos a descargar       [booleano]
  -r, --range    Límite de archivos a consultar separados por ':'
                                                          [cadena de caracteres]
```

## ⚙️ Configuración

Para configurar el cli, puede usar dos vías:

### Variables de entorno

*Debe crear un archivo .env en el mismo directorio en el que ejecute el comando.*

### Archivo JSON
*Puede pasar al comando un archivo de configuración con la bandera `-c`, indicando la ruta del archivo json, el cuál sobreescribirá las configuraciones del `.env` si es que existiesen*.

### Configuraciones disponibles

A continuación se describen las posibles configuraciones:


| Nombre | Descripción | Opcional |
|--|--|--|
| TENANT_ID | Id de tenant de la aplicación en Active Directory | 🚫 |
| CLIENT_ID | Id de cliente de la aplicación en Active Directory | 🚫 |
| DOWNLOAD_DIR | Directorio dónde se guardarán las descargas | ✅ |
| CHUNK_SIZE | Número de archivos para agrupar las descargas | ✅ |
| ONEDRIVE_USER | Si se especifica, no pedirá el login para inciar | ✅ |
| ONEDRIVE_PASS | Si se especifica, no pedirá el login para inciar | ✅ |
| DB_NAME | Si se especifica, nombrará la base de datos con la cadena especificada | ✅ |

*Para que la **autenticación por credenciales** funcione, deberá autorizar al menos una vez por dispositivo por lo que se recomienda agregar las credenciales después de usarlo al menos una vez.*

## Links
- [Js Sdk](https://github.com/microsoftgraph/msgraph-sdk-javascript)
- [Graph Explorer](https://developer.microsoft.com/en-us/graph/graph-explorer)
- [One Drive drives](https://learn.microsoft.com/en-us/graph/api/resources/drive?view=graph-rest-1.0)
#!/bin/bash

docker build -f docker/dockerfile . -t odbu:1.0.0

docker run --rm --name odbu -d odbu:1.0.0

docker cp odbu:/usr/src/odbu/odbu.7z ./odbu-ubuntu.7z

docker stop odbu

docker rmi odbu:1.0.0